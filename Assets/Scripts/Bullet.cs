﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public float speed=1;
    private Rigidbody2D thisRB;
    
    void Start()
    {
        thisRB = GetComponent<Rigidbody2D>();
        Destroy(gameObject, 1f);
    }

    void FixedUpdate() {
       thisRB.velocity = -transform.up * speed ;
    }

    void Update() {
      
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Player")
            {   
                GameManager.DrawChecker++;
                GameManager.hits.Add(new HitRegistrator(col.gameObject.GetComponent<PlayerControl>().isEnemy, System.DateTime.Now));  
            }
        if (col.gameObject.tag == "Bullet")
        {
            Destroy(gameObject);
        }

    }

}
