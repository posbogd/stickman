﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {
   
    public float nextFire;
    public float fireRate;
    public Transform shotSpawn;
    public GameObject shot;
    public bool isEnemy;
    public float moveStep;


    public float speed;
    private bool isMovingUp = false;
    private bool isMovingDown = false;
    public bool controledByAI;
    private Rigidbody2D rb;
  
    void Start () {
       rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	

    void Update() {


        if (controledByAI == false)
        {
            if (Input.touches.Length > 1) {
                MakeAShot();
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                MakeAShot();
            }

            if (isMovingUp)
            {
                rb.AddForce(new Vector2(0, 1f) * speed);
                //if (transform.position.y < yTopLimit) transform.position = new Vector3(transform.position.x, transform.position.y + moveStep, transform.position.z);
            }
            if (isMovingDown)
            {
                rb.AddForce(new Vector2(0, -1f) * speed);
                //   if (transform.position.y > yBotLimit) transform.position = new Vector3(transform.position.x, transform.position.y - moveStep, transform.position.z);
            }
        }
        else
        {
            ControledByAI();
        }
    }
    public void ControledByAI()
    {
        MakeAShot();
        float randomMove = Random.Range(0, 1f);
        if (randomMove >= 0.5f) rb.AddForce(new Vector2(0, 1f) * speed);
        else
            rb.AddForce(new Vector2(0, -1f) * speed);
    }
    public void MoveTopUp()
    {
        isMovingUp = false;
    }
    public void MoveBottomUP()
    {
        isMovingDown = false;
    }
    public void MoveTopDown()
    {
        isMovingUp = true;
    }
    public void MoveBottomDown()
    {
        isMovingDown = true;
    }
    public void MakeAShot() {

        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }
    }
   
}
