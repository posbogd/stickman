﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    public static int DrawChecker;
    public static GameManager controll;
    public static string gameResult;
    public static List<HitRegistrator> hits;
    public const float DRAW_SETUP = 0.5f;

    private Text endGameText;
    public GameObject popUpMenu;
    public static bool onPopUpMenu;
    void Awake()
    {
        OffPause();
        hits = new List<HitRegistrator>();
        onPopUpMenu = false;

        if (SceneManager.GetActiveScene().buildIndex != 0) endGameText = GameObject.Find("/Canvas/EndGameMenu/EndGameText").GetComponent<Text>();
    
        //Load();
        /*
        if (controll == null)
        {
            DontDestroyOnLoad(gameObject);
            controll = this;
        }
        else if (controll != this)
        {
            Destroy(gameObject);
        }     
        */
    }
    void Start()
    {
        //
       
    }

    void LateUpdate() {
        if (DrawChecker >= 1)
        {
            WinLoseManager();
          
        }
        if (onPopUpMenu) {
            if (popUpMenu != null) {
                endGameText.text = gameResult;
                popUpMenu.SetActive(true); }
        }
        else
        {
            if (popUpMenu != null) popUpMenu.SetActive(false);
        }
    }

    public static void EndGame()
    {   
        
        hits.Clear();
        DrawChecker = 0;
        Debug.Log(gameResult);
        onPopUpMenu = true;
        // PopUpMenu.SetActive(true);
        PauseGame();

    }

    public static void EndGameWon() {
        if (DrawChecker < 2)
        {
            gameResult="Victory!";
            EndGame();         
        }
        else Draw();
    }
    public static void EndGameLose()
    {
            gameResult = "Defeat.";
            EndGame(); 
    }
   public static void Draw()
    {
            gameResult = "Draw :|";
            EndGame();
    }

    public static void WinLoseManager() {
        if (hits.Count == 1)
        {
            if (hits[0].isEnemy) EndGameWon();
            else
                EndGameLose();
        }
        else if (hits.Count == 2)
        {
            TimeSpan diferenceTime = hits[0].currentTime.Subtract(hits[1].currentTime);
            if (diferenceTime.Seconds * diferenceTime.Seconds < DRAW_SETUP) Draw();
            else if (diferenceTime.Seconds < 0)
            {
                if (hits[0].isEnemy) EndGameWon();
                else
                    EndGameLose();
            }
            else if (diferenceTime.Seconds > 0)
            {
                if (hits[1].isEnemy) EndGameWon();
                else
                    EndGameLose();
            }
        }
    }
    public static void RestartGame() {
      
        SceneManager.LoadScene(1);
    }
    public static void MainMenu() {
        
        SceneManager.LoadScene(0);
    }

    public void RestartOnClick()
    {
        RestartGame();
    }
    public void MainMenuOnClick()
    {
        
        MainMenu();
    }
    public static void PauseGame() {
        Time.timeScale = 0;
    }
    public static void OffPause() {
        Time.timeScale = 1;
    }
}