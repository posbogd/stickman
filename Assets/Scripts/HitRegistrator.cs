﻿using System;

public struct HitRegistrator
    {
    public bool isEnemy;
    public DateTime currentTime;

    public HitRegistrator(bool isEnemy,DateTime currentTime){
        this.isEnemy = isEnemy;
        this.currentTime = currentTime;
        }
    }
